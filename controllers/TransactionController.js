
const crypt = require('../crypt');
const requestJson = require('request-json');
const syncRequest = require('sync-request');
const baseMlabURL = "https://api.mlab.com/api/1/databases/project-techu-shh/collections/";
const baseMlabURLRun = "https://api.mlab.com/api/1/databases/project-techu-shh/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

function createTransaction (req, res) {
  var functionName = "createTransaction";

  console.log(functionName + ": - " + "POST /apitechu/v1/transactions");

  var accountId = req.body.accountId;
  var amount = req.body.amount;
  console.log(functionName + ": - " + "accountId: " + accountId);
  console.log(functionName + ": - " + "description: " + req.body.description);
  console.log(functionName + ": - " + "type: " + req.body.type);
  console.log(functionName + ": - " + "amount: " + amount);

  // Se recupera el saldo anterior
  var currentBalance = getCurrentBalanceByAccountId(accountId);
  var newBalance = Number(currentBalance) + Number(amount);
  console.log(functionName + ": - " + "Current Balance: " + currentBalance);
  console.log(functionName + ": - " + "New Balance: " + newBalance);

  // Si el saldo final es negativo y el inporte es negativo no dejamos ejecutar el cargo
  if(newBalance<0 && amount<0){
    console.log(functionName + ": - " + "Balance negative. Current balance " + currentBalance);
    res.status(201).send ({"msg": "Balance negative. Current balance "+ currentBalance});
    return;
  }

  // Actualizamos el nuevo saldo
  updateBalanceByAccountId(accountId, newBalance);

  // Alta del movimiento
  var newTransaction = {
    "accountId": accountId,
    "description": req.body.description,
    "type": req.body.type,
    "transactionDate":new Date(),
    "amount": amount,
    "balance": newBalance
  };

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.post("Transaction?" + mLabAPIKEY, newTransaction,
    function( err, resMlab, body) {
      console.log(functionName + ": - " + "Transaction created");
      res.status(201).send ({"msg": "Transaction created"});
    }
  )
}

function getTransactionsByAccountId(req, res) {
  var functionName = "getTransactionsByAccountId";

  console.log(functionName + ": - " + "GET /apitechu/v1/transactions/:accountId");

  var accountId = req.params.accountId;
  console.log(functionName + ": - " + req.params.accountId);

  var query = 'q={"accountId":"' + accountId + '"}&s={"transactionDate":-1}';

  console.log(functionName + " - Query: " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("Transaction?" + query + "&" + mLabAPIKEY,
    function( err, resMlab, body) {
      if (err){
        var response = {
          "msg" :"Error obteniendo los movimientos de la cuenta"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" :"Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function getCurrentBalanceByAccountId(accountId){
  // La obtención del saldo se hace de forma sincrona.
  var functionName = "getCurrentBalanceByAccountId";
  var query = 'q={_id:{$oid:"' + accountId + '"}}';

  console.log(functionName + " - Query: " + query);
  console.log(baseMlabURL + "Account?" + query + "&" + mLabAPIKEY);

  var res = syncRequest('GET',baseMlabURL + "Account?" + query + "&" + mLabAPIKEY);
  if (res.error){
    console.log(functionName + ": - " + "Error gettions accounts.");
    throw Error("Error getting account details.");
  }
  if (res.getBody() != null ){
    var body = JSON.parse(res.getBody());
    var balance =  body[0].balance;
    console.log(functionName + ": - " + "Balance: " + balance);
    return balance;

  } else{
    throw Error("Account not found");
  }
}

function updateBalanceByAccountId(accountId, newBalance){
  // La actualización del saldo se hace de forma sincrona
  var functionName = "updateBalanceByAccountId";
  var object = { findAndModify : 'Account',
                  query : {_id : {$oid : accountId}},
                  update: { $set : {balance: newBalance}},
                  new: true,
                  upsert: false
                };

 var res = syncRequest('POST', baseMlabURLRun+"runCommand?" + mLabAPIKEY,
                          {json : object});
 var body = JSON.parse(res.getBody());
 return;
}


module.exports.getTransactionsByAccountId = getTransactionsByAccountId;
module.exports.createTransaction = createTransaction;
