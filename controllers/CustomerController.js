
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/project-techu-shh/collections/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

function createCustomer (req, res) {
  var functionName = "createCustomer";

  console.log(functionName + ": - " + "POST /apitechu/v1/customer");

  console.log(functionName + ": - " + "name: " + req.body.name);
  console.log(functionName + ": - " + "surname: " + req.body.surname);
  console.log(functionName + ": - " + "email: " + req.body.email);
  console.log(functionName + ": - " + "documentId: " + req.body.documentId);
  console.log(functionName + ": - " + "documentType: " + req.body.documentType);
  console.log(functionName + ": - " + "birthDate: " + req.body.birtDate);
  console.log(functionName + ": - " + "userId: " + req.body.userId);
  console.log(functionName + ": - " + "password: " + crypt.hash(req.body.password));

  var newCustomer = {
    "name": req.body.name,
    "surname":req.body.surname,
    "email": req.body.email,
    "documentId": req.body.documentId,
    "documentType": req.body.documentType,
    "birthDate": new Date (req.body.birthDate),
    "userId": req.body.userId,
    "password": crypt.hash(req.body.password),
    "longitude": req.body.longitude,
    "latitude": req.body.latitude
  };

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.post("Customer?" + mLabAPIKEY, newCustomer,
    function( err, resMlab, body) {
      console.log("Body"+ body);
      console.log(functionName + ": - " + "Customer created.");
      var customerId = body._id.$oid;
      console.log("customerId: " + customerId);
      var response = {
        "customerId": customerId
      };
      res.status(201).send (response);
    }
  )
}

/*
function getCustomerById(req, res) {
  var functionName = "getCustomerById";

  console.log(functionName + ": - " + "GET /apitechu/v1/customer/:customerId");

  var customerId = req.params.customerId;
  console.log(functionName + ": - " + req.params.customerId);

  var query = 'q={"_id" : { $oid: "' + customerId + '"} }';
  console.log(functionName + " - Query: " + query);

  var httpClient = requestJson.createClient(baseMlabURL);

  httpClient.get("Customer?" + query + "&" + mLabAPIKEY,
    function( err, resMlab, body) {
      if (err){
        var response = {
          "msg" :"Error getting customer details."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" :"Customer not found."
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}
*/

function getCustomerByUserId(req, res) {
  var functionName = "getCustomerByUserId";

  console.log(functionName + ": - " + "GET /apitechu/v1/customer/:userId");

  var userId = req.params.userId;
  console.log(functionName + ": - " + req.params.userId);

  var query = 'q={"userId" : "' + userId + '"}';

  var httpClient = requestJson.createClient(baseMlabURL);

  httpClient.get("Customer?" + query + "&" + mLabAPIKEY,
    function( err, resMlab, body) {
      if (err){
        var response = {
          "msg" :"Error getting customer details."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" :"Customer not found."
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

module.exports.createCustomer = createCustomer;
//module.exports.getCustomerById = getCustomerById;
module.exports.getCustomerByUserId = getCustomerByUserId;
