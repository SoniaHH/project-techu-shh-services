//const io = require('../io');
const crypt = require('../crypt');
const util = require('util');
const requestJson = require('request-json');
const syncRequest = require('sync-request');
const baseMlabURL = "https://api.mlab.com/api/1/databases/project-techu-shh/collections/";
const baseMlabURLRun = "https://api.mlab.com/api/1/databases/project-techu-shh/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

function createAccount (req, res) {
  var functionName = "createAccount";

  console.log(functionName + ": - " + "POST /apitechu/v1/accounts");

  console.log(functionName + ": - " + "description: " + req.body.description);
  console.log(functionName + ": - " + "type: " + req.body.type);
  console.log(functionName + ": - " + "customerId: " + req.body.customerId);

  var accountNumber = getAccountNumber();
  console.log(functionName + ": - " + "AccountNumber: "+accountNumber);

  var newAccount = {
    "accountNumber": accountNumber,
    "description": req.body.description,
    "type": req.body.type,
    "creationDate":new Date(),
    "balance": 0,
    "customerId": req.body.customerId,
    "status": "ACTIVE"
  };

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.post("Account?" + mLabAPIKEY, newAccount,
    function( err, resMlab, body) {
      console.log(functionName + ": - " + "Account created");
      res.status(201).send ({"msg": "Account created"});
    }
  )
}


function getAccountsByCustomerId(req, res) {
  var functionName = "getAccountsByCustomerId";

  console.log(functionName + ": - " + "GET /apitechu/v1/accounts/:customerId");

  var customerId = req.params.customerId;
  console.log(functionName + ": - " + req.params.customerId);

  var query = 'q={"customerId":"' + customerId + '"}';
  console.log(functionName + " - Query: " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log(baseMlabURL+"Account?" + query + "&" + mLabAPIKEY);
  httpClient.get("Account?" + query + "&" + mLabAPIKEY,
    function( err, resMlab, body) {
      if (err){
        var response = {
          "msg" :"Error getting customer accounts."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" :"Accounts not found."
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function getAccountNumber(){
  // La obtención de la secuencia se tiene que hacer de forma sincrona.
  var functionName = "getNextAccountNumber";
  var object = { findAndModify : 'Counters',
                  query : {_id : 'accountNumber'},
                  update: { $inc: {sequence_value:1}},
                  new: true,
                  upsert: true
                };

 var res = syncRequest('POST', baseMlabURLRun+"runCommand?" + mLabAPIKEY,
                          {json : object});
 var body = JSON.parse(res.getBody());
 var accountNumber =  body.value.sequence_value;

 console.log(functionName + ": - " + "New accountNumber: " + accountNumber);
 var accountFormat = "ES-0182-"+padDigits(accountNumber, 10);

 return accountFormat;
}

// Funcion de internet para formatear.
function padDigits(number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}

module.exports.createAccount = createAccount;
module.exports.getAccountsByCustomerId = getAccountsByCustomerId;
