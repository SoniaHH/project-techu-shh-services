
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/project-techu-shh/collections/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

function userLogin (req, res) {

  var functionName = "userLogin";
  var customerId = "";

  console.log(functionName + ": - " + "POST /apitechu/v1/login");
  var query = 'q={"userId": "' + req.body.userId + '"}';
  console.log(functionName + ": - " + "Password:["+crypt.hash(req.body.password)+"]");

  console.log(baseMlabURL + "Customer?" + query + "&" + mLabAPIKEY);
  httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("Customer?" + query + "&" + mLabAPIKEY,
  function(err, resMLab, body) {
    console.log(body);
    customerId = body[0]._id.$oid;
    var isPasswordcorrect =
      crypt.checkPassword(req.body.password, body[0].password);
    console.log(functionName + ": - " + "Password correct is " + isPasswordcorrect);
    if (!isPasswordcorrect) {
      var response = {
        "mensaje" : "Login/Password not found."
      }
      res.status(401);
      res.send(response);
    } else {
      console.log(functionName + ": - " + "Got a user with that email and password, logging in");
      query = 'q={"userId" : "' + body[0].userId +'"}';
      var putBody = '{"$set":{"logged":true}}';
      httpClient.put("Customer?" + query + "&" + mLabAPIKEY, JSON.parse(putBody),
        function(errPUT, resMLabPUT, bodyPUT) {
          console.log(functionName + ": - " + "PUT done");
          var response = {
            "customerId": customerId
          }
          res.send(response);
        }
      )
    }
  }
);
}


function userLogout(req, res) {
  var functionName = "userLogout";

  console.log(functionName + ": - " + "POST /apitechu/v1/logout/:userId");
  var userId = req.params.userId;
  var query = 'q={"userId": ' + userId + '}';

 console.log("query es " + query);
 httpClient = requestJson.createClient(baseMlabURL);

 httpClient.get("Customer?" + query + "&" + mLabAPIKEY,
   function( err, resMlab, body) {

     if (body.length == 0) {
       var response = {
         "msg" : "Logout error"
       }
       res.send(response);
     } else {
       console.log(functionName + ": - " + "Got a user with that id, logging out");
       query = 'q={"userId" : "' + userId +'"}';
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("Customer?" + query + "&" + mLabAPIKEY, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log(functionName + ": - " + "Loggout done");
           var response = {
             "msg" : "Logout correct"
           }
           res.send(response);
         }
       )
     }
   }
 );
}
//

module.exports.userLogin = userLogin;
module.exports.userLogout = userLogout;
