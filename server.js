//Coge la configuracion por defecto de dotenv con el config
require("dotenv").config();

const express = require('express');
const app = express();

var enableCORS = function(req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");
  next();
}

const port = process.env.PORT || 3000;
app.use(express.json());
app.use(enableCORS);

app.listen(port);
console.log("API escuchando en el puerto: " + port);

const CustomerController = require('./controllers/CustomerController');
const AccountController = require('./controllers/AccountController');
const TransactionController = require('./controllers/TransactionController');
const AuthenticationController = require('./controllers/AuthenticationController');

app.get('/apitchu/v1/hello',
  function(req, res)  {
    console.log("GET /apitchu/v1/hello");
    res.send({"msg" :"Hola desde API TechU"});
  }
)

app.post('/apitechu/v1/login', AuthenticationController.userLogin);
app.post('/apitechu/v1/logout/:userId', AuthenticationController.userLogout);

app.post('/apitechu/v1/customers',CustomerController.createCustomer);
app.get('/apitechu/v1/customers/:userId',CustomerController.getCustomerByUserId);

app.get('/apitechu/v1/accounts/:customerId',AccountController.getAccountsByCustomerId);
app.post('/apitechu/v1/accounts', AccountController.createAccount);

app.get('/apitechu/v1/transactions/:accountId',TransactionController.getTransactionsByAccountId);
app.post('/apitechu/v1/transactions',TransactionController.createTransaction);
