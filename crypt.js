const bcrypt = require("bcrypt");

//retorna un string
function hash(data) {
  console.log("Hashing data");

  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {

 console.log("Checking password");
 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);

}

//para enviarlo fuera ponemos exports
module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
